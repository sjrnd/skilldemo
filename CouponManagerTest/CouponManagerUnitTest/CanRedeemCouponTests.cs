using CouponManagerTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CouponManagerUnitTest
{
    [TestClass]
    public class CanRedeemCouponTests
    {
        // Mock the logger
        Mock<ILogger> _mockedLogger;

        public CanRedeemCouponTests()
        {
            _mockedLogger = new Mock<ILogger>();
        }

        #region Helper methods that could be moved to different class / file ...
        /// <summary>
        /// Conditionally get the coupon manager
        /// </summary>
        /// <param name="setupRetrieveMethod">to test after retrieval</param>
        /// <returns></returns>
        private CouponManager GetCouponManager(bool setupRetrieveMethod)
        {
            var mockedCouponProvider = new Mock<ICouponProvider>();

            if (setupRetrieveMethod)
            {
                mockedCouponProvider.Setup(m => m.Retrieve(It.IsAny<Guid>())).Returns(Task.FromResult(new Coupon()));
            }

            var mockedCouponManager = new Mock<CouponManager>(_mockedLogger.Object, mockedCouponProvider.Object);

            return mockedCouponManager.Object;
        }

        /// <summary>
        /// Conditionally get evaluators
        /// </summary>
        /// <param name="returnEmptyList">to get empty evaluators</param>
        /// <param name="allReturnTrue">to return evaluators with coupon evaluation result true/false</param>
        /// <returns></returns>
        private IEnumerable<Func<Coupon, Guid, bool>> GetEvaluators(bool returnEmptyList, bool allReturnTrue = false)
        {
            var mockedEvaluators = new Mock<List<Func<Coupon, Guid, bool>>>();

            if (returnEmptyList)
            {
                return mockedEvaluators.Object;
            }

            if (allReturnTrue)
            {
                Func<Coupon, Guid, bool> f2 = (c, u) => { return true; };
                mockedEvaluators.Object.Add(f2);
            }
            else
            {
                Func<Coupon, Guid, bool> f2 = (c, u) => { return false; };
                mockedEvaluators.Object.Add(f2);

                Func<Coupon, Guid, bool> f3 = (c, u) => { return false; };
                mockedEvaluators.Object.Add(f3);
            }

            return mockedEvaluators.Object;
        }

        #endregion

        [TestMethod]
        public async Task CanRedeemCoupon_Throws_ArgumentNullException_When_Evaluators_Is_Null()
        {
            // Arrange
            var couponManager = GetCouponManager(setupRetrieveMethod: false);

            // Act

            // Assert

            await Assert.ThrowsExceptionAsync<ArgumentNullException>(() =>
            {
                return couponManager.CanRedeemCoupon(Guid.NewGuid(), Guid.NewGuid(), null);
            });
        }

        [TestMethod]
        public async Task CanRedeemCoupon_Throws_KeyNotFoundException_When_Coupon_Not_Found()
        {
            // Arrange
            var evaluators = GetEvaluators(returnEmptyList: true);
            var couponManager = GetCouponManager(setupRetrieveMethod: false);

            // Act

            // Assert

            await Assert.ThrowsExceptionAsync<KeyNotFoundException>(() =>
            {
                return couponManager.CanRedeemCoupon(Guid.NewGuid(), Guid.NewGuid(), evaluators);
            });
        }

        [TestMethod]
        public void CanRedeemCoupon_Return_True_When_Coupon_Is_Found_And_Evaluators_Is_Empty()
        {
            // Arrange
            var evaluators = GetEvaluators(returnEmptyList: true);
            var couponManager = GetCouponManager(setupRetrieveMethod: true);

            // Act
            var result = couponManager.CanRedeemCoupon(Guid.NewGuid(), Guid.NewGuid(), evaluators).Result;

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CanRedeemCoupon_Returns_True_When_All_Evaluators_Return_True()
        {
            // Arrange
            var evaluators = GetEvaluators(returnEmptyList: false, allReturnTrue: true);
            var couponManager = GetCouponManager(setupRetrieveMethod: true);

            // Act
            var result = couponManager.CanRedeemCoupon(Guid.NewGuid(), Guid.NewGuid(), evaluators).Result;

            // Assert
            Assert.IsTrue(result);
        }

    }
}
