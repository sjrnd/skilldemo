﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to async programming demo!");
            var program = new Program();
            program.RunAsyncDemo();
        }

        private void RunAsyncDemo()
        {
            var tokenSource = new CancellationTokenSource();
            
            // Initiate async calls for 3 different resources with cancellationtoken
            Task<int> a = ReadHttpResourceAsync("https://www.trademe.co.nz", tokenSource.Token);
            Task<int> b = ReadHttpResourceAsync("https://gmail.com", tokenSource.Token);
            Task<int> c = ReadHttpResourceAsync("https://yahoo.com", tokenSource.Token);

            Console.WriteLine("Press c to cancel the operation");

            int total = 0;
            int resultCount = 0;


            // Continue with results
            a.ContinueWith((t) => { total += t.Result; resultCount++; });
            b.ContinueWith((t) => { total += t.Result; resultCount++; });
            c.ContinueWith((t) => { total += t.Result; resultCount++; });

            char input = '\0';
            do
            {
                Console.WriteLine($"{DateTime.Now.ToString("hh:hh:ss.fff")} -> Total in loop = {total} and resultCount = {resultCount}");
                Thread.Sleep(75); // just to avoid too many lines in console
                if (Console.KeyAvailable)
                {
                    input = Console.ReadKey().KeyChar;
                    if (input == 'c')
                    {
                        tokenSource.Cancel(); // request for cancellation
                    }
                }
            } while (resultCount < 3);

            Console.WriteLine($"Final total = {total}, resultCount = {resultCount}");
            Console.ReadLine();
        }

        private async Task<int> ReadHttpResourceAsync(string url, CancellationToken cancelToken)
        {
            try
            {

                var client = new HttpClient();
                //Invoke get call with cancellation token
                var response = await client.GetAsync(url, cancelToken);
                response.EnsureSuccessStatusCode();
                var responseBody = await response.Content.ReadAsStringAsync();
                return responseBody.Length; // content length returned
            }
            catch (OperationCanceledException oce)
            {
                Console.WriteLine($"\n{nameof(OperationCanceledException)} thrown\n");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Error while making http call :{e.Message} ");
            }

            return 0;
        }
    }
}
