﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CouponManager.Data;
using CouponManager.Data.Interfaces;
using CouponManager.Models.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CouponManager.Controllers
{
    [Produces("application/json")]
    [Route("api/CouponManager")]
    public class CouponManagerController : Controller
    {
        private readonly ICouponRepository _couponRepository;
        public CouponManagerController(ICouponRepository couponRepository)
        {
            _couponRepository = couponRepository;
        }

        /// <summary>
        /// Get all coupons
        /// </summary>
        /// <returns>list of coupons</returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _couponRepository.GetCoupons();
            return Ok(result);
        }

        /// <summary>
        /// Get all redemptions for particular offer type
        /// </summary>
        /// <param name="offerId">unique identifier of offer</param>
        /// <returns>Retrieves all redemeptions for passed offer id</returns>
        [HttpGet("GetRedemptionsByOffer/{offerid}")]
        public async Task<IActionResult> GetRedemptionsByOffer(Guid offerId)
        {
            if (offerId == null || offerId == Guid.Empty)
            {
                throw new ArgumentNullException("Invalid offer id");
            }

            var result = await _couponRepository.GetRedemptionsByOffer(offerId);
            return Ok(result);
        }

        /// <summary>
        /// use patch to update the redeem status
        /// </summary>
        /// <param name="coupon">unique identifier of coupon</param>
        /// <returns>returns updated coupon entity</returns>
        [HttpPatch("redeemcoupon/{id}")]
        public async Task<IActionResult> RedeemCoupon(Guid id)
        {
            if (id == null || id == Guid.Empty)
            {
                throw new ArgumentNullException("Invalid coupon id");
            }
            var result = await _couponRepository.RedeemCoupon(id);
            return Ok(result);
        }
       

        /// <summary>
        /// get active list of coupons
        /// </summary>
        /// <returns>retrieves all coupon with status active</returns>
        [HttpGet("getactivecoupons")]
        public async Task<IActionResult> GetActiveCoupons()
        {
            var result = await _couponRepository.GetActiveCoupons();
            return Ok(result);
        }

       /// <summary>
       /// use hardcoded coupon id test this method
       /// </summary>
       /// <param name="id">unique identifier of coupon</param>
       /// <param name="userId">unique identifier of user</param>
       /// <returns>boolean value for confirming if a coupon can be redeemed</returns>
        [HttpGet("canredeem/{id}/{userId}")]
        public async Task<IActionResult> CanRedeemCoupon(Guid id, Guid userId)
        {
            if (id == null || id == Guid.Empty || userId == null || userId == Guid.Empty)
            {
                throw new ArgumentNullException("Invalid coupon id");
            }
            var result = await _couponRepository.CanRedeemCoupon(id, userId);
            return Ok(result);
        }
    }
}