﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CouponManager.Models.Core
{
    public class CouponModel
    {
        public Guid Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsRedeemed { get; set; }
        public bool IsActive
        {
            get
            {
                return IsRedeemed ? false : (DateTime.Now > EndDate ? false : true);
            }
        }
        public string Title { get; set; }
        public Guid OfferId { get; set; }
        public Guid UserId { get; set; }
    }
}
