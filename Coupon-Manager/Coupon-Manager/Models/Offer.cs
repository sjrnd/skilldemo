﻿using System;
using System.Collections.Generic;

namespace CouponManager.Models
{
    public partial class Offer
    {
        public Offer()
        {
            Coupon = new HashSet<Coupon>();
        }

        public Guid Id { get; set; }
        public decimal DiscountPercentage { get; set; }
        public decimal LimitPerUser { get; set; }
        public decimal LimitOverall { get; set; }
        public string Title { get; set; }

        public virtual ICollection<Coupon> Coupon { get; set; }
    }
}
