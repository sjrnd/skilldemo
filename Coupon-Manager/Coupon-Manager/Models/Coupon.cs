﻿using System;
using System.Collections.Generic;

namespace CouponManager.Models
{
    public partial class Coupon
    {
        public Guid Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsRedeemed { get; set; }
        public Guid OfferId { get; set; }
        public Guid UserId { get; set; }

        public virtual Offer Offer { get; set; }
    }
}
