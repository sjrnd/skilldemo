﻿using CouponManager.Data.Interfaces;
using CouponManager.Models;
using CouponManager.Models.Core;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CouponManager.Data
{
    public class CouponRepository : ICouponRepository
    {
        public async Task<IEnumerable<CouponModel>> GetCoupons()
        {
            using (var context = new TestDBContext())
            {
                var coupons = await (from c in context.Coupon
                               select new CouponModel
                               {
                                   Id = c.Id,
                                   StartDate = c.StartDate,
                                   EndDate = c.EndDate,
                                   IsRedeemed = c.IsRedeemed,
                                   Title = c.Offer.Title,
                                   UserId = c.UserId,
                                   OfferId = c.Offer.Id
                               }).ToListAsync();
                return coupons;
            }
        }

        public async Task<bool> CanRedeemCoupon(Guid id, Guid userId)
        {
            using (var context = new TestDBContext())
            {
                var coupon = await (from c in context.Coupon where c.Id == id && c.UserId == userId select c).FirstOrDefaultAsync();
                if (coupon == null)
                {
                    throw new KeyNotFoundException("Invalid coupon");
                }
                return coupon.IsRedeemed == false && coupon.EndDate >= DateTime.Now;

            }
        }
        public async Task<CouponModel> RedeemCoupon(Guid id)
        {
            using (var context = new TestDBContext())
            {
                var coupon = (from c in context.Coupon where c.Id == id && c.IsRedeemed == false select c).FirstOrDefault();
                if (coupon == null)
                {
                    throw new KeyNotFoundException("Invalid coupon");
                }
                coupon.IsRedeemed = true;
                await context.SaveChangesAsync();
                var couponUpdated = (from c in context.Coupon
                                    where c.Id == coupon.Id
                                    select new CouponModel
                                    {
                                        Id = c.Id,
                                        StartDate = c.StartDate,
                                        EndDate = c.EndDate,
                                        IsRedeemed = c.IsRedeemed,
                                        Title = c.Offer.Title,
                                        UserId = c.UserId,
                                        OfferId = c.Offer.Id
                                    }).FirstOrDefault();
                return couponUpdated;
            }

        }

        public async Task<IEnumerable<CouponModel>> GetActiveCoupons()
        {
            using (var context = new TestDBContext())
            {
                var coupons = await (from c in context.Coupon
                               join o in context.Offer on c.OfferId equals o.Id
                               where c.IsRedeemed == false && c.EndDate >= DateTime.Now
                               select new CouponModel
                               {
                                   Id = c.Id,
                                   StartDate = c.StartDate,
                                   EndDate = c.EndDate,
                                   IsRedeemed = c.IsRedeemed,
                                   Title = o.Title,
                                   UserId = c.UserId,
                                   OfferId = o.Id
                               }).ToListAsync();

                return coupons;
            }
        }

        public async Task<IEnumerable<CouponModel>> GetRedemptionsByOffer(Guid offerId)
        {
            using (var context = new TestDBContext())
            {
                var offer = (from o in context.Offer where o.Id == offerId select o).FirstOrDefault();
                if (offer == null)
                {
                    throw new KeyNotFoundException("Invalid offer id");
                }
                var coupons = await (from c in context.Coupon
                               where c.OfferId == offerId
                               select new CouponModel
                               {
                                   Id = c.Id,
                                   StartDate = c.StartDate,
                                   EndDate = c.EndDate,
                                   IsRedeemed = c.IsRedeemed,
                                   Title = c.Offer.Title,
                                   UserId = c.UserId,
                                   OfferId = c.Offer.Id,
                               }).ToListAsync();

                return coupons;
            }

        }
    }
}
