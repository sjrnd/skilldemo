﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CouponManager.Models.Core;

namespace CouponManager.Data.Interfaces
{
    public interface ICouponRepository 
    {
        Task<IEnumerable<CouponModel>> GetCoupons();
        Task<IEnumerable<CouponModel>> GetActiveCoupons();
        Task<IEnumerable<CouponModel>> GetRedemptionsByOffer(Guid offerId);
        Task<CouponModel> RedeemCoupon(Guid id);
        Task<bool> CanRedeemCoupon(Guid id, Guid userId);

    }
}
