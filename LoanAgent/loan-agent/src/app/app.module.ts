import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoanInputComponent } from './loan-input/loan-input.component';
import { LoanTermsComponent } from './loan-terms/loan-terms.component';
import { LoanContactComponent } from './loan-contact/loan-contact.component';
import { MatInputModule } from '@angular/material';


@NgModule({
  declarations: [
    AppComponent,
    LoanInputComponent,
    LoanTermsComponent,
    LoanContactComponent,
  ],
  imports: [
    BrowserModule,
    MatInputModule,
  ],
  exports: [
    MatInputModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
