import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanContactComponent } from './loan-contact.component';

describe('LoanContactComponent', () => {
  let component: LoanContactComponent;
  let fixture: ComponentFixture<LoanContactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanContactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
