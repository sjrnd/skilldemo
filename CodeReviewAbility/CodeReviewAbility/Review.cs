﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CodeReviewAbility
{
    /// <summary>
    /// Provides capabilities for managing a customers itinerary.
    /// </summary>
    public class ItineraryManager
    {
        private readonly IDataStore _dataStore;
        private readonly IDistanceCalculator _distanceCalculator;
        // <Sudhakar>  dependency injection is not used and code is less testable
        public ItineraryManager()
        {
            // <Sudhakar> magic strings can be moved to  global constants 
            _dataStore = new SqlAgentStore(ConfigurationManager.ConnectionStrings["SqlDbConnection"].ConnectionString);
            _distanceCalculator = new GoogleMapsDistanceCalculator(ConfigurationManager.AppSettings["GoogleMapsApiKey"]);
        }

        /// <summary>
        /// Calculates a quote for a customers itinerary from a provided list of airline providers.
        /// </summary>
        /// <param name="itineraryId">The identifier of the itinerary</param>
        /// <param name="priceProviders">A collection of airline price providers.</param>
        /// <returns>A collection of quotes from the different airlines.</returns>
        /// 

        // <Sudhakar> use async instead of blocking code
        public IEnumerable<Quote> CalculateAirlinePrices(int itineraryId, IEnumerable<IAirlinePriceProvider> priceProviders)
        {
            
            var itinerary = _dataStore.GetItinaryAsync(itineraryId).Result;
            if (itinerary == null) // <sudhakar> use paranthesis wherever it is applicable
                throw new InvalidOperationException(); // <Sudhakar> use keynotfoundexception

            // <sudhakar> use var for results
            List<Quote> results = new List<Quote>();

            // <sudhakar> Do null check to avoid NullReferenceException 
            Parallel.ForEach(priceProviders, provider =>
            {
                var quotes = provider.GetQuotes(itinerary.TicketClass, itinerary.Waypoints);
                foreach (var quote in quotes)
                    results.Add(quote);
            });
            return results;
        }

        /// <summary>
        /// Calculates the total distance traveled across all waypoints in a customers itinerary.
        /// </summary>
        /// <param name="itineraryId">The identifier of the itinerary</param>
        /// <returns>The total distance traveled.</returns>
        public async Task<double> CalculateTotalTravelDistanceAsync(int itineraryId)
        {
            var itinerary = await _dataStore.GetItinaryAsync(itineraryId);
            if (itinerary == null)
                throw new InvalidOperationException(); // <Sudhakar> use keynotfoundexception
            double result = 0;

            // <Sudhakar> do null check on itinerary.Waypoints to avoid NullReferenceException
            for (int i = 0; i < itinerary.Waypoints.Count - 1; i++)
            {
                // <Sudhakar> change to result +=
                result = result + _distanceCalculator.GetDistanceAsync(itinerary.Waypoints[i],
                     itinerary.Waypoints[i + 1]).Result;
            }
            return result;
        }

        /// <summary>
        /// Loads a Travel agents details from Storage
        /// </summary>
        /// <param name="id">The id of the travel agent.</param>
        /// <param name="updatedPhoneNumber">If set updates the agents phone number.</param>
        /// <returns>The travel agent if located, otherwise null.</returns>
        public TravelAgent FindAgent(int id, string updatedPhoneNumber)
        {
            var agentDao = _dataStore.GetAgent(id);

            // <Sudhakar> throw keynotfoundexception
            if (agentDao == null)
                return null;
            if (!string.IsNullOrWhiteSpace(updatedPhoneNumber))
            {
                agentDao.PhoneNumber = updatedPhoneNumber;

                // <Sudhakar> Have UpdateAgent to return true/false and handle response accordingly
                _dataStore.UpdateAgent(id, agentDao);
            }
            return Mapper.Map<TravelAgent>(agentDao);
        }
    }

}
